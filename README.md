# Image Elasticsearch

Image Elastic search 
- Elastic 6.2.3
- X-pack

- Bas� sur Centos 7.4

- Produit install� dans `/opt/elasticsearch`

- Utilise `systemd` pour le lancement
- Utilisateur `elastic : uid:2001`

# Utilisation de l'image

## Ports
- `9200` Port client elasticsearch
- `9300` Port synchro inter noeuds

## Points de montage
- logs : `/es/logs`
- config: `/es/config`
- data: `/es/data

